# podcastr

## Aplicação desenvolvida no NLW#05 ministrada pela RocketSeat

<p>
  Para rodar em modo de desenvolvimento é necessário rodar o comando <code>yarn dev</code> para iniciar a aplicação em modo de
  desenvolvimento e <code>yarn server</code> para rodar a API.
</p>

<p>Qual será o próximo nível?</p>
<ul>
  <li>Alternância entre o modo Dark e o modo Light</li>
  <li>Responsividade</li>
</ul>